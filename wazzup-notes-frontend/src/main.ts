import Vue from 'vue';
import App from '@/App.vue';
import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-default.css';

import router from '@/router'

Vue.use(VueToast, {
  position: 'top-right'
});

Vue.config.productionTip = false;

new Vue({
  router,
  render: (h) => h(App),
}).$mount('#app');
