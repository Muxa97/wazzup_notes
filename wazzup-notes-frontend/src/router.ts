import Vue from 'vue'
import Router, { RouteConfig } from 'vue-router'

Vue.use(Router)

export const routes: RouteConfig[] = [
  {
    path: '/login',
    component: () => import('@/views/login/index.vue'),
    meta: {
      title: 'Wazzup notes login'
    }
  },
  {
    path: '/registration',
    component: () => import('@/views/registration/index.vue'),
    meta: {
      title: 'Wazzup notes registration'
    }
  },
  {
    path: '/',
    component: () => import('@/views/main/index.vue'),
    meta: {
      title: 'Wazzup notes'
    }
  },
  {
    path: '/note/:hash',
    component: () => import('@/views/editNote/index.vue'),
    meta: {
      title: 'Wazzup notes'
    }
  },
  {
    path: '*',
    redirect: '/'
  }
]


const createRouter = () => new Router({
  mode: 'history',
  scrollBehavior: (to, from, savedPosition) => {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  },
  base: process.env.BASE_URL || 'localhost:8080',
  routes: routes
})

const router = createRouter()

router.beforeEach((to, from, next) => {
  next()
})

router.afterEach((to, from) => {
  document.title = to.meta.title
})

export default router
