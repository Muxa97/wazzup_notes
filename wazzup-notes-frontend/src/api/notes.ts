import axios from 'axios'
import {INote} from "@/api/types";

export const getNotes = async (params: {page: number, limit: number}) => {
  return axios({
    url: `http://${process.env.VUE_APP_API_BASE_URL}/notes?page=${params.page}&limit=${params.limit}`,
    withCredentials: true,
    method: 'get'
  })
}

export const saveNote = async (params: { text: string }) => {
  return axios({
    url: `http://${process.env.VUE_APP_API_BASE_URL}/notes/new`,
    method: 'put',
    withCredentials: true,
    data: params
  })
}

export const getNoteByHash = async (params: { hash: string }) => {
  return axios({
    url: `http://${process.env.VUE_APP_API_BASE_URL}/notes/${params.hash}`,
    method: 'get',
    withCredentials: true
  })
}

export const deleteNote = async (params: { id: number }) => {
  return axios({
    url: `http://${process.env.VUE_APP_API_BASE_URL}/notes/${params.id}`,
    method: 'delete',
    withCredentials: true
  })
}

export const updateNote = async (params: INote) => {
  return axios({
    url: `http://${process.env.VUE_APP_API_BASE_URL}/notes/${params.hash}`,
    method: 'post',
    withCredentials: true,
    data: params
  })
}

export const logout = async () => {
  return axios({
    url: `http://${process.env.VUE_APP_API_BASE_URL}/user/logout`,
    withCredentials: true,
    method: 'post'
  })
}
