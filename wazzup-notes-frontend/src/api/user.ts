import axios from 'axios'
import Cookie from 'js-cookie'

export const register = async (params: {login: string, password: string}) => {
  return axios({
    url: `http://${process.env.VUE_APP_API_BASE_URL}/user/register`,
    method: 'put',
    withCredentials: true,
    data: params
  })
}

export const login = async (params: {login: string, password: string}) => {
  return axios({
    url: `http://${process.env.VUE_APP_API_BASE_URL}/user/login`,
    method: 'post',
    withCredentials: true,
    data: params
  })
}

export const logout = async () => {
  const response = await axios({
    url: `http://${process.env.VUE_APP_API_BASE_URL}/user/logout`,
    withCredentials: true,
    method: 'post'
  })
  Cookie.remove('token')
  return response
}
