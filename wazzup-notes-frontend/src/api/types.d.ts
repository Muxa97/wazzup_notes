
export interface IUser {
  id: number,
  login: string,
  password: string
}

export interface INote {
  id: number,
  hash: string,
  text: string,
  createdAt: Date,
  updatedAt: Date,
  editable: boolean
}
