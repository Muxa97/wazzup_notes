# Wazzup notes backend

To start project

    npm install
    npm run db:migrate
    npm start

## Requirements

* Node.js ^10.0.0
* Globally installed sequelize-cli
* MySQL

Server runs on localhost
