'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    queryInterface.createTable('users', {
      id: {
        type: Sequelize.DataTypes.INTEGER.UNSIGNED,
        primaryKey: true,
        autoIncrement: true,
      },
      login: {
        type: Sequelize.DataTypes.STRING,
        field: 'login',
        allowNull: false,
      },
      password: {
        type: Sequelize.DataTypes.STRING,
        field: 'password',
        allowNull: false,
      }
    })

    queryInterface.createTable('auth_tokens', {
      id: {
        type: Sequelize.DataTypes.INTEGER.UNSIGNED,
        primaryKey: true,
        autoIncrement: true,
      },
      token: {
        type: Sequelize.DataTypes.STRING,
        field: 'token',
        allowNull: false,
      },
      ttl: {
        type: Sequelize.DataTypes.INTEGER,
        field: 'ttl',
        allowNull: false
      },
      userId: {
        type: Sequelize.DataTypes.INTEGER.UNSIGNED,
        field: 'user_id',
        allowNull: false
      },
      createdAt: {
        type: Sequelize.DATE,
        field: 'created_at',
        allowNull: true,
      },
      updatedAt: {
        type: Sequelize.DATE,
        field: 'updated_at',
        allowNull: true,
      },
    })

    queryInterface.createTable('notes', {
      id: {
        type: Sequelize.DataTypes.INTEGER.UNSIGNED,
        primaryKey: true,
        autoIncrement: true,
      },
      text: {
        type: Sequelize.DataTypes.STRING,
        allowNull: false,
      },
      hash: {
        type: Sequelize.DataTypes.UUID,
        allowNull: false
      },
      userId: {
        type: Sequelize.DataTypes.INTEGER.UNSIGNED,
        field: 'user_id',
        allowNull: false
      },
      createdAt: {
        type: Sequelize.DATE,
        field: 'created_at',
        allowNull: true,
      },
      updatedAt: {
        type: Sequelize.DATE,
        field: 'updated_at',
        allowNull: true,
      },
    })
  },

  down: async (queryInterface, Sequelize) => {
    queryInterface.dropTable('users')
    queryInterface.dropTable('auth_tokens')
    queryInterface.dropTable('notes')
  }
};
