process.env.NODE_ENV = 'testing'

const chai = require('chai')
const chaiHttp = require('chai-http')
const faker = require('faker')
const axios = require('axios')
const app = require('../src/app')

chai.use(chaiHttp)
chai.should()

describe("REST API tests", () => {

    it('should register new user', function(done) {
        const testUser = {
            login: faker.random.word() + '1234',
            password: faker.random.word() + '1234'
        }
        chai.request(app)
            .put('/user/register')
            .send(testUser)
            .end((err, res) => {
                res.text.should.be.equal('Registered')
                done()
            })
    })

    it('should get all notes for user', function(done) {
        this.timeout(4000)
        chai.request(app)
            .get('/notes')
            .query({ limit: 10, page: 1 })
            .end((err, res) => {
                res.body.should.have.property('notes')
                res.body.should.have.property('total')
                res.body.notes.should.be.an('array')
                done()
            })
    })

    it('should create new note and delete it', function(done) {
        this.timeout(4000)
        chai.request(app)
            .put('/notes/new')
            .send({ text: 'Test' })
            .end((err, res) => {
                res.body.should.have.property('id')
                res.body.should.have.property('hash')
                chai.request(app)
                    .delete('/notes/' + res.body.id)
                    .end((err, res) => {
                        res.text.should.be.equal('Deleted')
                        done()
                    })
            })
    })

    it('should create and update note', function(done) {
        this.timeout(4000)
        let hash
        chai.request(app)
            .put('/notes/new')
            .send({ text: 'Test' })
            .end((err, res) => {
                res.body.should.have.property('id')
                res.body.should.have.property('hash')
                hash = res.body.hash
                chai.request(app)
                    .post('/notes/' + res.body.hash)
                    .send({ text: 'Test 2'})
                    .end((err, res) => {
                        res.body.should.have.property('hash')
                        res.body.should.have.property('text')
                        res.body.hash.should.be.equal(hash)
                        res.body.text.should.be.equal('Test 2')
                        done()
                    })
            })
    })
})
