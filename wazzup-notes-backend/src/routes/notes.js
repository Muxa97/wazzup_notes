const { notes } = require('../controllers')
const checkAuth = require('../middleware/checkAuth.js')
const { AuthToken, User } = require('../models')

module.exports = (router) => {
    router.put('/notes/new', checkAuth, async (req, res) => {
        const { body } = req
        const uid = req.user.id

        try {
            const note = await notes.createNote(uid, body)

            res.send(note)
        } catch (error) {
            console.error('Notes module error: PUT /notes/new', error)
            res.send({ error: error.message })
        }
    })

    router.get('/notes', checkAuth, async (req, res) => {
        const { page, limit } = req.query
        const offset = (page - 1) * limit
        const uid = req.user.id

        try {
            const records = await notes.find(uid, offset, +limit)

            res.send({ notes: records.rows, total: records.count })
        } catch (error) {
            console.error('Notes module error: GET /notes', error)
            res.send({ error: error.message })
        }
    })

    router.delete('/notes/:id', checkAuth, async (req, res) => {
        const { id } = req.params

        try {
            await notes.deleteNote(id)

            res.send('Deleted')
        } catch (error) {
            console.error('Notes module error: DELETE /notes/:id', error)
            res.send({ error: error.message })
        }
    })

    router.get('/notes/:hash', async (req, res) => {
        const { hash } = req.params
        const { token } = req.cookies

        try {
            const note = await notes.getNote(hash)
            const noteObj = { ...note }
            noteObj.editable = true

            if (!token) {
                noteObj.editable = false
            } else {
                const authToken = await AuthToken.findOne({
                    where: {
                        value: token
                    }
                })
                const user = await User.findOne({
                    where: { id: authToken.userId }
                })
                if (!user || user.id !== note.userId) {
                    noteObj.editable = false
                }
            }

            res.send(noteObj)
        } catch (error) {
            console.error('Notes module error: GET /notes/:hash', error)
            res.send({ error: error.message })
        }
    })

    router.post('/notes/:hash', checkAuth, async (req, res) => {
        const { hash } = req.params
        const uid = req.user.id
        const { body } = req

        try {
            const updatedNote = await notes.updateNote(uid, hash, body)

            res.send(updatedNote)
        } catch (error) {
            console.error('Notes module error: POST /notes/:hash', error)
            res.send({ error: error.message })
        }
    })
}
