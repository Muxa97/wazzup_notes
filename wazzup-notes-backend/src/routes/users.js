const { users } = require('../controllers')

module.exports = (router) => {
    router.put('/user/register', async (req, res) => {
        const { body } = req

        try {
            if (body.login.length < 6 || body.password.length < 6) {
                throw Error('Validation error')
            }

            const token = await users.createUser(body)

            res.cookie('token', token.value, { maxAge: token.ttl, httpOnly: false, domain: 'localhost' })
            res.send('Registered')
        } catch (error) {
            console.error('Users module error PUT /user/register: ', error)
            res.send({ error: error.message })
        }
    })

    router.post('/user/login', async (req, res) => {
        const { login, password } = req.body

        try {
            const token = await users.loginUser(login, password)

            res.cookie('token', token.value, { maxAge: token.ttl, httpOnly: false, domain: 'localhost' })
            res.send('Sign in')
        } catch (error) {
            console.error('Users module error POST /user/login: ', error)
            res.send({ error: error.message })
        }
    })

    router.post('/user/logout', async (req, res) => {
        const { token } = req.cookies

        try {
            await users.logoutUser(token)

            res.send('logout')
        } catch (error) {
            console.error('Users module error POST /user/logout: ', error)
            res.send({ error: error.message })
        }
    })
}
