const express = require('express')

const router = express.Router()

require('./users.js')(router)
require('./notes.js')(router)

module.exports = router
