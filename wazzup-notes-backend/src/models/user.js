const { Model } = require('sequelize')

class User extends Model {
    static init (sequelize, DataTypes){
        return super.init({
            id: {
                type: DataTypes.INTEGER.UNSIGNED,
                primaryKey: true,
                autoIncrement: true,
            },
            login: {
                type: DataTypes.STRING,
                field: 'login',
                allowNull: false,
            },
            password: {
                type: DataTypes.STRING,
                field: 'password',
                allowNull: false,
            },
        }, {
            timestamps: false,
            tableName: 'users',
            sequelize,
        })
    }
}

module.exports = User
