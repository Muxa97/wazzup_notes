const Sequelize = require('sequelize')

class Note extends Sequelize.Model {
    static init (sequelize, DataTypes){
        return super.init({
            id: {
                type: DataTypes.INTEGER.UNSIGNED,
                primaryKey: true,
                autoIncrement: true,
            },
            text: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            hash: {
                type: DataTypes.UUID,
                defaultValue: Sequelize.UUIDV4,
            },
            userId: {
                type: DataTypes.INTEGER.UNSIGNED,
                field: 'user_id',
                allowNull: false
            }
        }, {
            timestamps: true,
            tableName: 'notes',
            sequelize,
        })
    }
}

module.exports = Note
