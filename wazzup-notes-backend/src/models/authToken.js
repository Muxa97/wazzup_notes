const  Sequelize = require('sequelize')

class AuthToken extends Sequelize.Model {
    static init (sequelize, DataTypes){
        return super.init({
            id: {
                type: DataTypes.INTEGER.UNSIGNED,
                primaryKey: true,
                autoIncrement: true,
            },
            value: {
                type: DataTypes.UUID,
                field: 'token',
                defaultValue: Sequelize.UUIDV4,
            },
            ttl: {
                type: DataTypes.INTEGER,
                field: 'ttl',
                allowNull: false,
                defaultValue: 1000 * 60 * 60 * 24
            },
            userId: {
                type: Sequelize.DataTypes.INTEGER.UNSIGNED,
                field: 'user_id',
                allowNull: false
            }
        }, {
            timestamps: true,
            tableName: 'auth_tokens',
            sequelize,
        })
    }
}

module.exports = AuthToken
