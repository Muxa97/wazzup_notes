const Sequelize = require('sequelize')
const sequelize = require('../config/db.js')
const User = require('./user.js')
const AuthToken = require('./authToken.js')
const Note = require('./note.js')

const models = {
    User: User.init(sequelize, Sequelize),
    AuthToken: AuthToken.init(sequelize, Sequelize),
    Note: Note.init(sequelize, Sequelize)
}

User.hasMany(AuthToken)
User.hasMany(Note)

const db = {
    ...models,
    sequelize,
}

module.exports = db
