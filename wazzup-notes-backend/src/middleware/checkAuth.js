const { AuthToken, User } = require('../models')

module.exports = async (req, res, next) => {
    if (process.env.NODE_ENV === 'testing') {
        const user = await User.findOne({
            where: { login: 'qwertyqwe' }
        })

        req.user = user
        next()
    } else {

        const {token} = req.cookies

        try {
            if (!token) {
                throw new Error('You are not signed in')
            }

            const authToken = await AuthToken.findOne({
                where: {
                    value: token
                }
            })

            const lifeTime = Date.now() - (new Date(authToken.createdAt)).getTime()
            if (lifeTime > authToken.ttl) {
                await authToken.destroy()
                res.status(401).send('Authentication error')
            }

            const user = await User.findOne({
                where: {id: authToken.userId}
            })
            if (!user) {
                await authToken.destroy()
                res.status(401).send('Authentication error')
            }

            req.user = user
            next()
        } catch (error) {
            res.status(401).send('Authentication error')
        }
    }
}
