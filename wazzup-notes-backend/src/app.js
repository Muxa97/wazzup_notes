require('dotenv').config()
const express = require('express')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const cors = require('cors')
const router = require('./routes')

const PORT = process.env.PORT || 3000
const app = express()

app.use(cookieParser())
app.use(bodyParser.json())
app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', 'http://localhost:' + (process.env.APP_PORT || 8080));
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    next();
});

app.use(router)

app.listen(PORT, () => {
    console.log(`Server started on ${PORT}`)
})

module.exports = app
