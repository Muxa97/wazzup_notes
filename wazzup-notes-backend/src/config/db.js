const Sequelize = require('sequelize')

const db = process.env.NODE_ENV === 'testing' ? process.env.TEST_DB : process.env.MYSQL_DB
const dbUser = process.env.NODE_ENV === 'testing' ? process.env.TEST_DB_USER : process.env.MYSQL_USER
const dbPwd = process.env.NODE_ENV === 'testing' ? process.env.TEST_DB_PWD : process.env.MYSQL_PWD

const sequelize = new Sequelize(
    db,
    dbUser,
    dbPwd,
    {
        host: process.env.MYSQL_HOST,
        port: process.env.MYSQL_PORT,
        dialect: 'mysql',
        define: {
            charset: 'utf8',
            collate: 'utf8_general_ci',
            timestamps: true,
            underscored: true,
        },
    },
)
module.exports = sequelize
