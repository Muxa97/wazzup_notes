const { User, AuthToken } = require('../models')

const createUser = async (data) => {
    const [ user, created ] = await User.findOrCreate({
        where: {
            login: data.login,
            password: data.password
        },
        default: data
    })

    if (!created) {
        throw new Error('Not unique login')
    }

    const authToken = await AuthToken.create({
        userId: user.id
    })

    return authToken
}

const loginUser = async (login, password) => {
    const user = await User.findOne({
        where: {
            login, password
        }
    })

    if (!user) {
        throw new Error('Invalid login or password')
    }

    const authToken = await AuthToken.create({
        userId: user.id
    })

    return authToken
}

const logoutUser = async (token) => {
    const authToken = await AuthToken.findOne({
        where: {
            value: token
        },
    })
    const user = await User.findOne({
        where: {
            id: authToken.userId
        }
    })

    await AuthToken.destroy({
        where: {
            userId: user.id
        }
    })
}

module.exports = {
    createUser,
    loginUser,
    logoutUser
}
