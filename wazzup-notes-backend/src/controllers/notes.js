const { Note } = require('../models')

const createNote = async (uid, data) => {
    data.userId = uid

    if (!data.text) {
        throw new Error('Invalid request body')
    }

    return Note.create(data)
}

const find = async (uid, offset, limit) => {
    return Note.findAndCountAll({
        where: {
            userId: uid
        },
        offset,
        limit
    })
}

const deleteNote = async (id) => {
    if (!id) {
        throw new Error('Note id not provided')
    }

    return Note.destroy({
        where: {
            id
        }
    })
}

const getNote = async (hash) => {
    if (!hash) {
        throw new Error('Note hash not provided')
    }

    return Note.findOne({
        where: {
            hash
        }
    })
}

const updateNote = async (uid, hash, body) => {
    if (!hash || !body.text) {
        throw new Error('Invalid request body')
    }

    const note = await Note.findOne({ where: { hash, userId: uid } })

    if (!note) {
        throw Error('Note doesn\'t exist or you can\'t update it')
    }

    await note.update({
        text: body.text
    })

    return note
}

module.exports = {
    createNote,
    find,
    deleteNote,
    getNote,
    updateNote
}
