const users = require('./users')
const notes = require('./notes')

module.exports = {
    users,
    notes
}
